---
title: "Contato"
date: 2020-10-21T21:06:34-03:00
draft: false
type: page
---

Olá, essa são as melhores maneiras de entrar em contato comigo, escolha a que você esteja mais familiar em usar.

# Email

Meu email é [eu @ ksantana.net](mailto:eu @ ksantana.net). 

Tambem possuo uma chave PGP/GPG caso deseje mandar uma mensagem criptografada de pontoa a ponta

[Você pode encontrar ela aqui](https://keys.openpgp.org/vks/v1/by-fingerprint/8C9273C4817AFA38074DBF7F92938423B960D703), ou via id: rsa4096/8C9273C4817AFA38074DBF7F92938423B960D703

# Matrix

Estou presente no mensageiro instantâneo como @khalilsantana:matrix.org.

# IRC

Estou presente no libera.chat como khalil.

# Discord

Sou o khalil.santana no Discord.
